//
//  MqttManager.swift
//  NIPeekaboo
//
//  Created by 吉田拓人 on 2021/03/15.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import SwiftMQTT

class MqttManager {
    
    let mqttSession: MQTTSession
    var connectionState: Bool = false
    
    init(host: String, port: Int){
        self.mqttSession = MQTTSession(
            host: host,
            port: UInt16(port),
            clientID: "swift", // must be unique to the client
            cleanSession: true,
            keepAlive: 15,
            useSSL: false
        )
        
        self.mqttSession.connect { error in
            if error == .none {
                print("Connected!")
                self.connectionState = true
            } else {
                print(error.description)
            }
        }
    }
    
    func publish(topic: String, distance: String, direction: String) {
        let json = ["distance":distance,"direction":direction]
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
//        print(topic, data)
        
        self.mqttSession.publish(data, in: topic, delivering: .atLeastOnce, retain: false) { error in
            if error == .none {
                print("Published data in \(topic)!")
            } else {
                print(error.description)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
